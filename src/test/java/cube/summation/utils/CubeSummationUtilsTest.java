package cube.summation.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CubeSummationUtilsTest {
	
	/** 
	 * the redis join char. 
	 */
	public static final String IS_NUMERIC= "28";
	
	/** 
	 * the redis join char. 
	 */
	public static final String IS_NOT_NUMERIC= "AB";
	
	/** 
	 * the redis join char. 
	 */
	public static final String REDIS_JOIN_CHAR= "1-2-3";
	
	/** 
	 * the redis join char. 
	 */
	public static final String X_NUMBER= "1";
	
	/** 
	 * the redis join char. 
	 */
	public static final String Y_NUMBER= "2";
	
	/** 
	 * the redis join char. 
	 */
	public static final String Z_NUMBER= "3";
	
	/** 
	 * the redis join char. 
	 */
	public static final int STRING= 3;
	
	/** 
	 * the redis join char. 
	 */
	public static final String STRING_EQUALS= "3";
	
	/** 
	 * the redis join char. 
	 */
	public static final String INTEGER= "25";
	
	/** 
	 * the redis join char. 
	 */
	public static final int INTEGER_EQUALS= 25;
	
	/** 
	 * the redis join char. 
	 */
	CubeSummationUtils cubeSummationUtils = new CubeSummationUtils();
	
	
	@Test
    public void isNumericTest() {
        boolean isNumeric = cubeSummationUtils.isNumeric(IS_NUMERIC);
        assertTrue(isNumeric);
    }
	
	@Test
    public void isNotNumericTest() {
        boolean isNumeric = cubeSummationUtils.isNumeric(IS_NOT_NUMERIC);
        assertFalse(isNumeric);
    }
	
	@Test
    public void buildKeyTest() {
        String key = cubeSummationUtils.buildKey(X_NUMBER, Y_NUMBER, Z_NUMBER);
        assertEquals(REDIS_JOIN_CHAR, key);
    }
	
	@Test
    public void getIntegerTest() {
        int integer = cubeSummationUtils.getInteger(INTEGER);
        assertEquals(INTEGER_EQUALS, integer);
    }
	
	@Test
    public void getStringTest() {
        String string = cubeSummationUtils.getString(STRING);
        assertEquals(STRING_EQUALS, string);
    }
}
