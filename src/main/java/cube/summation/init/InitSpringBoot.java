package cube.summation.init;

import org.springframework.boot.SpringApplication;

import cube.summation.controllers.CubeSummationController;

/**
 * Represents the Init Spring Boot 
 * 
 * @author <a href="f_alejo2004@hotmail.com">Alejandro Murcia</a>
 * @since 0.0.1    
 */
public class InitSpringBoot {

	public static void main(String[] args) throws Exception {
        SpringApplication.run(CubeSummationController.class, args);
    }
}
