package cube.summation.constants;

/**
 * Represents the Cube Summation Action Type 
 * 
 * @author <a href="f_alejo2004@hotmail.com">Alejandro Murcia</a>
 * @since 0.0.1    
 */
	public class CubeSummationRedisKeys {
		
		/** 
		 * the redis host. 
		 */
		public static final String STATE= "state";
		
		/** 
		 * the redis host. 
		 */
		public static final String MATRIX_LENGTH= "matrixLength";
		/** 
		 * the redis host. 
		 */
		public static final String REMAINING_CASE_TEST= "remainingCaseTest";
		
		/** 
		 * the redis host. 
		 */
		public static final String REMAINING_OPERATIONS= "remainingOperations";
	}
