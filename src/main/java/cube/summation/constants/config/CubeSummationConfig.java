package cube.summation.constants.config;

/**
 * Represents the Cube Summation Constants Configuration
 * 
 * @author <a href="f_alejo2004@hotmail.com">Alejandro Murcia</a>
 * @since 0.0.1    
 */
public class CubeSummationConfig {
	
	/** 
	 * the redis host. 
	 */
	public static final String REDIS_HOST= "localhost";
}