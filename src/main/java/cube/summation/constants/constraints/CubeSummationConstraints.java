package cube.summation.constants.constraints;

/**
 * Represents the Cube Summation Constants Constraints
 * 
 * @author <a href="f_alejo2004@hotmail.com">Alejandro Murcia</a>
 * @since 0.0.1    
 */
public class CubeSummationConstraints {
	
	/** 
	 * the redis host. 
	 */
	public static final int MAX_TEST_CASES= 50;
	
	/** 
	 * the redis host. 
	 */
	public static final int MAX_LENGTH_MATRIX= 100;
	
	/** 
	 * the redis host. 
	 */
	public static final int MAX_NUMBER_OPERATIONS= 1000;
}