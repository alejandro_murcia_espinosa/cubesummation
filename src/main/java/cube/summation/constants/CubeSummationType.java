package cube.summation.constants;

/**
 * Represents the Cube Summation Action Type 
 * 
 * @author <a href="f_alejo2004@hotmail.com">Alejandro Murcia</a>
 * @since 0.0.1    
 */
public enum CubeSummationType {
	
	/** 
	 * The update action
	 */
	UPDATE,
	/** 
	 * The query action
	 */
	QUERY;
}
