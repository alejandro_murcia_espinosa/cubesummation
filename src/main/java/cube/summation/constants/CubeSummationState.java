package cube.summation.constants;

/**
 * Represents the Cube Summation Action Type 
 * 
 * @author <a href="f_alejo2004@hotmail.com">Alejandro Murcia</a>
 * @since 0.0.1    
 */
public enum CubeSummationState {
	
	/** 
	 * The select state
	 */
	SELECT_TEST_CASES,
	/** 
	 * The define state
	 */
	DEFINE_MATRIX_AND_OPERATIONS,
	/** 
	 * The exec query state
	 */
	EXEC_QUERY;
}
