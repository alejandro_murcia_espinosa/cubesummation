package cube.summation.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import cube.summation.models.CubeSummation;
import cube.summation.models.CubeSummationResponse;
import cube.summation.services.ICubeSummationService;
import cube.summation.utils.CubeSummationUtils;
/**
 * Represents the Cube Summation Service
 * 
 * @author <a href="f_alejo2004@hotmail.com">Alejandro Murcia</a>
 * @since 0.0.1    
 */
@Service
@ComponentScan("cube.summation.utils")
public class CubeSummationService implements ICubeSummationService{
	
	/** 
	 * the redis split char. 
	 */
	public static final String REDIS_SPLIT_CHAR= " ";
	
	/** 
	 * The cube summation utils. 
	 */
	@Autowired
	private CubeSummationUtils cubeSummationUtils;
	
	/**
	 * Return the response
	 * @param arrayRequest
	 * @param cubeSummation
	 * 
	 * @return the response
	 */
	public ResponseEntity<CubeSummationResponse> validateRequest(String stringRequest, CubeSummation cubeSummation){
		String response = null;
		String[] arrayRequest = getRequest(stringRequest);
		if(arrayRequest.length>0){
			response = cubeSummationUtils.getResponse(arrayRequest, cubeSummation);
		}
		
		CubeSummationResponse cubeSummationResponse = new CubeSummationResponse(200, response);
		return new ResponseEntity<CubeSummationResponse>(cubeSummationResponse, HttpStatus.OK);
	}

	/**
	 * Return the array request
	 * @param stringRequest
	 * 
	 * @return the array request
	 */
	private String[] getRequest(String stringRequest) {
		return stringRequest.split(REDIS_SPLIT_CHAR);
	}
	
}