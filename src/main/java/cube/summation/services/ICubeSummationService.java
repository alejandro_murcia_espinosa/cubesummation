package cube.summation.services;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import cube.summation.models.CubeSummation;
import cube.summation.models.CubeSummationResponse;

/**
 * Represents the Cube Summation Service
 * 
 * @author <a href="f_alejo2004@hotmail.com">Alejandro Murcia</a>
 * @since 0.0.1    
 */
@Service
public interface ICubeSummationService {
	
	
	/**
	 * Validate Request Method 
	 * @param cubeSummation 
	 * @param the stringRequest
	 * @return the ApiResponse
	 */
	ResponseEntity<CubeSummationResponse> validateRequest(String stringRequest, CubeSummation cubeSummation);
	
}