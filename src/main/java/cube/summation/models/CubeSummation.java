package cube.summation.models;

import redis.clients.jedis.Jedis;
import cube.summation.constants.CubeSummationRedisKeys;
import cube.summation.constants.CubeSummationState;
import cube.summation.utils.RedisClient;

/**
 * The cube summation model
 *
 * @author <a href="f_alejo2004@hotmail.com">Alejandro Murcia</a>
 * @since 0.0.1
 */
public class CubeSummation {
	
    /**  
     * The redis Client 
     */
    private RedisClient redisClient;

    

	/**  
     * The constructor 
     */
    public CubeSummation(Jedis jedis, CubeSummationState state) {
    	this.redisClient = new RedisClient(jedis);
    	setState(state);
	}

	/**
     * Gets the state.
     *
     * @return state
     */
    public CubeSummationState getState() {
		return CubeSummationState.valueOf(this.redisClient.get(CubeSummationRedisKeys.STATE));
	}

    /**
     * Sets the state.
     *
     * @param state
     */
	public void setState(CubeSummationState state) {
		this.redisClient.add(CubeSummationRedisKeys.STATE, state.toString());
	}

	/**
     * Gets the matrix_length.
     *
     * @return the matrix_length
     */
	public String getMatrixLength() {
		return this.redisClient.get(CubeSummationRedisKeys.MATRIX_LENGTH);
	}

	/**
     * Sets the matrix_length.
     *
     * @param matrix_length
     */
	public void setMatrixLength(String matrixLength) {
		this.redisClient.add(CubeSummationRedisKeys.MATRIX_LENGTH, matrixLength);
	}

	/**
     * Gets the remaining_case_test.
     *
     * @return the remaining_case_test
     */
	public String getRemainingCaseTest() {
		return this.redisClient.get(CubeSummationRedisKeys.REMAINING_CASE_TEST);
	}
	
	/**
     * Sets the remaining_case_test.
     *
     * @param remaining_case_test
     */
	public void setRemainingCaseTest(String remainingCaseTest) {
		this.redisClient.add(CubeSummationRedisKeys.REMAINING_CASE_TEST, remainingCaseTest);
	}

	/**
     * Gets the remaining_operations.
     *
     * @return the remaining_operations
     */
	public String getRemainingOperations() {
		return this.redisClient.get(CubeSummationRedisKeys.REMAINING_OPERATIONS);
	}

	/**
     * Sets the remaining_operations.
     *
     * @param remaining_operations
     */
	public void setRemainingOperations(String remainingOperations) {
		this.redisClient.add(CubeSummationRedisKeys.REMAINING_OPERATIONS, remainingOperations);
	}
	
	/**
     * Gets the redisClient.
     *
     * @return the redisClient
     */
	public RedisClient getRedisClient() {
		return redisClient;
	}

	/**
     * Sets the redisClient.
     *
     * @param redisClient
     */
	public void setRedisClient(RedisClient redisClient) {
		this.redisClient = redisClient;
	}

}