package cube.summation.models;

/**
 * This response for the cube summation service
 *
 * @author <a href="f_alejo2004@hotmail.com">Alejandro Murcia</a>
 * @since 0.0.1
 */
public class CubeSummationResponse {

    /** The message id. */
    private int messageId;

    /** The message. */
    private String message;

    /**
     * Instantiates a new sms service response.
     *
     * @param message the API reply message
     */
    public CubeSummationResponse(String message) {
        this.message = message;
    }

    /**
     * Instantiates a new sms service response.
     *
     * @param messageId the message id
     * @param message the API reply message
     */
    public CubeSummationResponse(int messageId, String message) {
        this.messageId = messageId;
        this.message = message;
    }

    /**
     * Gets the message id.
     *
     * @return the messageId
     */
    public int getMessageId() {
        return messageId;
    }

    /**
     * Sets the message id.
     *
     * @param messageId the message id to set
     */
    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the message.
     *
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
