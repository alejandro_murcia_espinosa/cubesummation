package cube.summation.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import redis.clients.jedis.Jedis;
import cube.summation.constants.CubeSummationState;
import cube.summation.constants.config.CubeSummationConfig;
import cube.summation.models.CubeSummation;
import cube.summation.models.CubeSummationResponse;
import cube.summation.services.ICubeSummationService;

/**
 * This controller provides the access for the cube summation service
 *
 * @author <a href="f_alejo2004@hotmail.com">Alejandro Murcia</a>
 * @since 0.0.1
 */
@Controller
@EnableAutoConfiguration
@ComponentScan("cube.summation.services")
public class CubeSummationController {
	
	/**
	 * the cubeSummation
	 */
	private final CubeSummation cubeSummation;
	
	/** 
	 * The cube summation service. 
	 */
	@Autowired
	private ICubeSummationService cubeSummationService;
	
	/**
	 * the constructor
	 * init jedis conection instance of redis
	 */
	CubeSummationController(){
		Jedis jedis = new Jedis(CubeSummationConfig.REDIS_HOST);
		jedis.flushAll();
		cubeSummation = new CubeSummation(jedis, CubeSummationState.SELECT_TEST_CASES);
	}
	
	
	/**
	 * Gets the cubeSummation
	 *
	 * @param message the message
	 * @return the cubeSummation response
	 */
    @RequestMapping(value = "/cubeSummation", method = RequestMethod.POST)
    @CrossOrigin(origins = "*", maxAge = 3600)
    @ResponseBody
    public ResponseEntity<CubeSummationResponse> cubeSummation(@RequestBody String message) {
    	
    	return cubeSummationService.validateRequest(message, cubeSummation);
    }
}