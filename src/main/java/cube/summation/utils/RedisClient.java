package cube.summation.utils;

import redis.clients.jedis.Jedis;


/**
 * Represents the Redis Client
 * 
 * @author <a href="f_alejo2004@hotmail.com">Alejandro Murcia</a>
 * @since 0.0.1    
 */
public class RedisClient {
	
	/**
	 * The Jedis Instance
	 *
	 */
	private Jedis jedis;
	
	/**
	 * The Constructor
	 *
	 */
	public RedisClient(Jedis jedis){
		this.jedis = jedis; 
		
	}
	/**
	 * add redisobject.
	 *
	 * @param id key of the object
	 */
	public void add(String key, String value) {
		jedis.set(key, value);
	}
	
	/**
	 * Remove object from redis.
	 *
	 * @param id key of the object to be removed
	 */
	public void remove(String key) {
		jedis.del(key);
	}
	
	/**
	 * get redisobject.
	 *
	 * @param id key of the object
	 */
	public String get(String key) {
		return jedis.get(key);
	}
	
	/**
	 * flusAll redis.
	 *
	 */
	public void flusAll() {
		jedis.flushAll();
	}
}
