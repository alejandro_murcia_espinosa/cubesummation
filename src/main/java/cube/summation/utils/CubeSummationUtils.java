package cube.summation.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import cube.summation.constants.CubeSummationState;
import cube.summation.constants.CubeSummationType;
import cube.summation.constants.constraints.CubeSummationConstraints;
import cube.summation.models.CubeSummation;

/**
 * Represents the Cube Summation Constants Configuration
 * 
 * @author <a href="f_alejo2004@hotmail.com">Alejandro Murcia</a>
 * @since 0.0.1    
 */
@Component
public class CubeSummationUtils {
	
	/** 
	 * the redis join char. 
	 */
	public static final String REDIS_JOIN_CHAR= "-";
	
	/**
	 * Return the response by CubeSummationState
	 * @param arrayRequest
	 * @param cubeSummation
	 * 
	 * @return the response
	 */
	public String getResponse(String[] arrayRequest,
			CubeSummation cubeSummation) {
		String returnResponse = null;
		try {
			switch(cubeSummation.getState()){
				case SELECT_TEST_CASES:
					validateTestCaseState(arrayRequest, cubeSummation);
					break;
				case DEFINE_MATRIX_AND_OPERATIONS:
					validateDefineMatrixOperationsState(arrayRequest, cubeSummation);
					break;
				case EXEC_QUERY:
					returnResponse = validateExecQueryState(arrayRequest, cubeSummation);
					break;	
				default:
				break;
			}
			return returnResponse;
		} catch (Exception e) {
			return null;
		}
		
	}
	
	/**
	 * validate Test Case State
	 * @param arrayRequest
	 * @param cubeSummation
	 * 
	 */
	private void validateTestCaseState(String[] arrayRequest, CubeSummation cubeSummation) {
		if(arrayRequest.length == 1 && 
				getInteger(arrayRequest[0]) < CubeSummationConstraints.MAX_TEST_CASES){
			cubeSummation.setRemainingCaseTest(arrayRequest[0]);	
			cubeSummation.setState(CubeSummationState.DEFINE_MATRIX_AND_OPERATIONS);
		}		
	}
	
	/**
	 * validate Define Matrix Operations State
	 * @param arrayRequest
	 * @param cubeSummation
	 */
	private void validateDefineMatrixOperationsState(String[] arrayRequest, CubeSummation cubeSummation) {
		if(arrayRequest.length == 2 && 
				getInteger(cubeSummation.getRemainingCaseTest())>0 &&
				getInteger(arrayRequest[0]) < CubeSummationConstraints.MAX_LENGTH_MATRIX &&
				getInteger(arrayRequest[1]) < CubeSummationConstraints.MAX_NUMBER_OPERATIONS){
			cubeSummation.setMatrixLength(arrayRequest[0]);	
			cubeSummation.setRemainingOperations(arrayRequest[1]);	
			cubeSummation.setState(CubeSummationState.EXEC_QUERY);
		}		
	}
	
	/**
	 * validate Exec Query State by CubeSummationType
	 * @param arrayRequest
	 * @param cubeSummation
	 * 
	 * @return the response
	 */
	private String validateExecQueryState(String[] arrayRequest, CubeSummation cubeSummation) {
		String returnResponse = null;
		if((arrayRequest.length == 5 || arrayRequest.length == 7) &&
				getInteger(cubeSummation.getRemainingOperations()) > 0 &&
				CubeSummationType.valueOf(arrayRequest[0]) != null){
			
			switch(CubeSummationType.valueOf(arrayRequest[0])){
				case UPDATE:
					updateOperation(arrayRequest, cubeSummation);
					break;
				case QUERY:
					returnResponse = queryOperation(arrayRequest, cubeSummation);
					break;
				default:
				break;
			}
			updateCubeSubmission(cubeSummation);
		}
		return returnResponse;
	}
	
	/**
	 * the update operation CubeSummationType
	 * @param arrayRequest
	 * @param cubeSummation
	 */
	private void updateOperation(String[] arrayRequest, CubeSummation cubeSummation) {
		if(arrayRequest.length == 5 && validateUpdateArrayRequest(arrayRequest, cubeSummation)
				){
			updateRedisRegister(arrayRequest, cubeSummation);
		}		
	}
	
	/**
	 * the validate for update CubeSummationType
	 * @param arrayRequest
	 * @param cubeSummation
	 */
	private boolean validateUpdateArrayRequest(String[] arrayRequest, CubeSummation cubeSummation) {
		int matrixLength = getInteger(cubeSummation.getMatrixLength());
		return validateUpdateMatrixPoint(arrayRequest[1], matrixLength) &&
				validateUpdateMatrixPoint(arrayRequest[2], matrixLength) &&
				validateUpdateMatrixPoint(arrayRequest[3], matrixLength) &&
				isNumeric(arrayRequest[4]); 
	}
	
	/**
	 * validate the update matrix point
	 * @param pointA
	 * @param matrixLength
	 *  
	 * @return the response
	 */
	private boolean validateUpdateMatrixPoint(String pointA, int matrixLength) {
		return getInteger(pointA) <= matrixLength && getInteger(pointA) > 0 ; 
	}
	
	/**
	 * the update Redis Register for update CubeSummationType
	 * @param arrayRequest
	 * @param cubeSummation
	 */
	private void updateRedisRegister(String[] arrayRequest,
			CubeSummation cubeSummation) {
		String key = buildKey(arrayRequest[1], arrayRequest[2], arrayRequest[3]);
		String value = arrayRequest[4];
		cubeSummation.getRedisClient().add(key, value);
	}

	/**
	 * the query operation CubeSummationType
	 * @param arrayRequest
	 * @param cubeSummation
	 * 
	 * @return the response
	 */
	private String queryOperation(String[] arrayRequest, CubeSummation cubeSummation) {
		String returnResponse = null;
		if(arrayRequest.length == 7 && validateQueryArrayRequest(arrayRequest, cubeSummation)
				){
			returnResponse = getQuery(arrayRequest, cubeSummation);
		}		
		return returnResponse;
	}
	
	/**
	 * the validate for query CubeSummationType
	 * @param arrayRequest
	 * @param cubeSummation
	 */
	private boolean validateQueryArrayRequest(String[] arrayRequest, CubeSummation cubeSummation) {
		int matrixLength = getInteger(cubeSummation.getMatrixLength());
		return validateQueryMatrixPoints(arrayRequest[1], arrayRequest[4], matrixLength) &&
				validateQueryMatrixPoints(arrayRequest[2], arrayRequest[5], matrixLength) &&
				validateQueryMatrixPoints(arrayRequest[3], arrayRequest[6], matrixLength); 
	}
	
	/**
	 * get query for query operation CubeSummationType
	 * @param arrayRequest
	 * @param cubeSummation
	 * 
	 * @return the response
	 */
	private String getQuery(String[] arrayRequest,
			CubeSummation cubeSummation) {
		String returnResponse = null;
		int x1 = getInteger(arrayRequest[1]);
		int y1 = getInteger(arrayRequest[2]);
		int z1 = getInteger(arrayRequest[3]);
		
		int x2 = getInteger(arrayRequest[4]);
		int y2 = getInteger(arrayRequest[5]);
		int z2 = getInteger(arrayRequest[6]);
		
		int response = 0;
		try {
			for (int x = x1; x <= x2; x++) {
				for (int y = y1; y <= y2; y++) {
					for (int z = z1; z <= z2; z++) {
						response += getRedisValue(x,y,z, cubeSummation.getRedisClient());
					}
				}
			}
			returnResponse = getString(response);
		} catch (Exception e) {
			returnResponse = null;
		}
		
		return returnResponse;
	}

	/**
	 * get redis value from integers
	 * @param x param
	 * @param y param
	 * @param z param
	 * @param redisClient
	 * 
	 * @return the redisIntValue
	 */
	private int getRedisValue(int x, int y, int z, RedisClient redisClient) {
		int redisIntValue = 0;
		String redisValue = redisClient.get(
				buildKey(getString(x), getString(y), getString(z)));
		
		if(redisValue != null){
			redisIntValue = getInteger(redisValue);
		}		
		
		return redisIntValue;
	}
	
	/**
	 * validate the query matrix points
	 * @param pointA
	 * @param pointB
	 * @param matrixLength
	 *  
	 * @return the response
	 */
	private boolean validateQueryMatrixPoints(String pointA,String pointB, int matrixLength) {
		return getInteger(pointA) <= matrixLength && getInteger(pointA) > 0 &&
				getInteger(pointB) <= matrixLength && getInteger(pointB) > 0 &&
				getInteger(pointA) <= getInteger(pointB) ; 
	}
	
	/**
	 * update Cube Submission after operation by CubeSummationType
	 * @param cubeSummation
	 */
	private void updateCubeSubmission(CubeSummation cubeSummation) {

		int remainingOperations = getInteger(cubeSummation.getRemainingOperations()) - 1;
		cubeSummation.setRemainingOperations(
				Integer.toString(
						remainingOperations
				)
		);
		
		if(remainingOperations == 0){
			int remainingCaseTest = getInteger(cubeSummation.getRemainingCaseTest()) - 1;
			cubeSummation.setRemainingCaseTest(
					Integer.toString(
							remainingCaseTest
					)
			
			);
			cubeSummation.getRedisClient().flusAll();
			if(remainingCaseTest == 0){
				cubeSummation.setState(CubeSummationState.SELECT_TEST_CASES);
			}else{
				cubeSummation.setState(CubeSummationState.DEFINE_MATRIX_AND_OPERATIONS);
				cubeSummation.setRemainingCaseTest(Integer.toString(
						remainingCaseTest));
			}
		}
		
	}	
		
	/**
	 * validate if a String is integer
	 * @param cadena
	 *  
	 * @return the boolean result
	 */	
	public boolean isNumeric(String string) {

        boolean result;

        try {
            Integer.parseInt(string);
            result = true;
        } catch (NumberFormatException excepcion) {
        	result = false;
        }

        return result;
    }
		
	/**
	 * build key for redis get
	 * @param x
	 * @param y
	 * @param z  
	 * 
	 * @return the key
	 */
	public String buildKey(String x, String y, String z){
		return StringUtils.join(x,REDIS_JOIN_CHAR,
				y, REDIS_JOIN_CHAR, z);
	}
	
	/**
	 * Conver String to int
	 * @param integer
	 *  
	 * @return the integer
	 */
	public int getInteger(String string){
		return Integer.parseInt(string);
	}
	
	/**
	 * Conver int to String
	 * @param int
	 *  
	 * @return the String
	 */
	public String getString(int integer){
		return Integer.toString(integer);
	}
}
